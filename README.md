# README #

DrawBox Scheme

### What is this repository for? ###

* Some task for create working example describes author skills in JS/JQ/PHP/AJAX/JSON/XML

* Version: 0.7 (20.07.2014)
--Includes: raw JS (lib) / JSON parse / JSON forms / CSS / HTML / JS Touch events
--
--Add touch events in dragging
--Misc changes in tables creation

* Version: 0.6 (19.07.2014)
--
--Includes: raw JS (lib) / JSON parse / JSON forms / CSS / HTML
--Instantiate more than one object
--Simple top menu with button
--Multiple tables creation (simple test way)
--Some bug fixes

* Version: 0.5 (17.07.2014)
--
--Includes: raw JS (lib) / JSON parse / JSON forms / CSS / HTML
--Add instantiation from a string at preload or on demand ($dbox().Instantiate.fromString([string],[true/false = alert window]))
--Some bug fixes

* Version: 0.4 (14.07.2014)
--
--Includes: raw JS (lib) / JSON parse / JSON forms / CSS / HTML
--Edit inner objects, Delete inner objects, Description modal window, Simple Modal window
--DOM parser update, Forms getter / setter update

* Version: 0.3 (07.07.2014)
--
--Includes: raw JS (lib) / JSON parse / JSON forms / CSS / HTML
--Some object dragging, objects add inner elements
--Code DOM parser, modal windows, form creation from JSON, form buttons

* Version: 0.1 (02.07.2014)
--Includes: raw JS (lib) / CSS / HTML

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* All questions to Repin Anton  dire.one@gmail.com