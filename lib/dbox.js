(function () {

	var $dbox = function(data){
		return new dboxClass(data);
	};

	var $dboxIterator = null;
	var $dboxDragObject = null;

	var dboxClass = function(data){

		var itsCurdata = data;
		this.version = '0.7';
		this.curData = data;
		this.curData = 1;
		this.mainClock = null;

		this.Elements = {
			sessionIdField:'dbox-fn-session',dataIdField:'dbox-fn-dataload',
			layerOpen:'div',layerClose:'/div',layerId:'id=',layerClass:'class=',
			inputOpen:'input',textOpen:'textarea',formOpen:'form',selectOpen:'select',
			layerRoot:'dbox-fn-body',layerWork:'dbox-fn-work',
			tableRoot:'dbox-fn-table-object',tableWrap:'dbox-fn-table-object-in',
			tableDefId:'tab',tableTitle:'dbox-fn-table-object-title',
			btnAdd:'dbox-fn-button-add-calm',btnAddHover:'dbox-fn-button-add-hover',
			btnAddClick:'dbox-fn-button-add-press',btnSimple:'dbox-fn-simple-btn',
			windowModal:'dbox-fn-modal-window-frame',windowModalIn:'dbox-fn-modal-window-frame-in',
			windowModalClose:'dbox-fn-modal-window-frame-close',
            ctrlElem:'dbox-fn-control-field', ctrlMenu:'dbox-fn-control-field-in', ctrlBtn: 'dbox-fn-control-btn'
		};

        this.callFunctions = {
            buttonTableCreate: function(){dboxClass().Actions.addTable();}
        };


        this.ctrlMenu = {

            createMenu: function(mpos,mvis,data){

                var dbox = new dboxClass();
                var menuObj = dbox.Markup.formElement(dbox.Elements.layerOpen,dbox.Elements.ctrlElem,dbox.Elements.ctrlElem,'','');
                var menuObjIn = dbox.Markup.formElement(dbox.Elements.layerOpen,dbox.Elements.ctrlMenu,dbox.Elements.ctrlMenu,'','');
                var menuBtnAddTab = dbox.Markup.btnForm(dbox.Elements.ctrlBtn,dbox.Elements.ctrlBtn,'Add new table',dbox.callFunctions.buttonTableCreate);

                dbox.Markup.addLayer(menuObjIn,1,menuBtnAddTab);
                dbox.Markup.addLayer(menuObj,1,menuObjIn);
                this.relocateMenu(menuObj,mpos);
                dbox.Markup.addLayer("",0,menuObj);

            },

            relocateMenu: function(menuObj,posEdge){
                var inObj = menuObj.childNodes[0];
                var inWidth = dboxCoordsReplacePx(window.getComputedStyle(inObj,null).getPropertyValue('width'));
                var poses = {top:[-4,'50%',0,-(inWidth/2)]};

                if(typeof(menuObj) === 'object'){
                    for (var p in poses){
                        if(p == posEdge){
                            menuObj.style.top = poses[p][0];
                            menuObj.style.left = poses[p][1];
                            menuObj.style.marginTop = poses[p][2];
                            menuObj.style.marginLeft = poses[p][3];
                        }
                    }
                }
            }
        }

        // LOAD wait for DOM preaparation and fires default init function or something custom after that

		this.Load = {

			dbx: function(){return new dboxClass();},
			pageState: false,
			pageTimer: null,
			iterTime: 100,
			iterCount: 0,

			PageLoad: function(){
												console.log('PageLoad Execute ok');
				dbxInstanceObj = this.dbx();
				id = document.getElementById(dbxInstanceObj.Elements.sessionIdField);
				did = document.getElementById(dbxInstanceObj.Elements.dataIdField);
				if(id != null)
				{
												console.log('PageLoad Pass ID ok');
					dataExisted = false;
					if(did != null && did.value.length > 0){
						dataExisted = true;
					}

					dbxInstanceObj.Markup.SessId = id.value;
					dbxInstanceObj.Markup.init(dataExisted,dbxInstanceObj);
				}
			},

			PageCompleted: function(state){
												console.log('PageCompleted Execute ok');
				if(state)
				{
					this.PageLoad();
				}
				return state;
			},

			CompleteCheck: function(){
				this.iterCount++;
				if(document.readyState === "complete")
				{
												console.log('CompleteCheck ReadyState ok');
					this.pageState = true;
					clearInterval($dboxIterator);
					dbxInstance = new dboxClass();
					dbxInstance.Load.PageCompleted(this.pageState);
				}
				if(this.iterCount>1000)
				{
					clearInterval(dboxIterator);
					dbxInstance = new dboxClass();
					state = dbx.Load.PageCompleted(this.pageState);
				}
			},

			PageInit: function(){
				$dboxIterator = setInterval(this.CompleteCheck,this.iterTime);
			},

			PageResize: function(ident){
				id = document.getElementById(this.dbx().Elements.layerWork+'-'+ident);
				wdt=id.offsetWidth;
				hgt=id.offsetHeight;
				if(wdt<100 || hgt <100)
				{
					id.style.width = "100%";
					id.style.height = "100%";
				}
			}
		}

		// MARKUP initialize default window content and consists from functions which create dom entity

		this.Markup = {

			dbx: function(){return new dboxClass();},
			SessId: null,

			init: function(dataExisted,dbx){

				if(this.SessId != null)
					{
					console.log('Init with session number: '+ this.SessId);
						dbxEl = dbx.Elements;
						dbxMp = dbx.Markup;

						inlayer = this.formLayer(dbxEl.layerWork+'-'+this.SessId,dbxEl.layerWork,'','','');
						layer = this.formLayer(this.SessId,dbxEl.layerRoot,'','','');
						this.addLayer("",0,layer);
						this.addLayer(layer,1,inlayer);
						dbx.Load.PageResize(this.SessId);

						if(!dataExisted)
						{
							defblockt = dbxMp.tableBlock(inlayer,'','','','','',true);

							defblocke = defblockt.intableObj;
							defblockid = defblockt.tableId;
							defblockt = defblockt.tableObj;

							deftitle = dbxMp.titleAdd(dbxEl.tableTitle+'-'+dbxEl.tableDefId+defblockid,dbxEl.tableTitle,dbxEl.tableDefId);
							defbtn = dbxMp.btnAdd(dbxEl.btnAdd+'-'+dbxEl.tableDefId+defblockid,dbxEl.btnAdd,defblocke);

							this.addLayer(inlayer,1,defblockt);
							this.addLayer(defblocke,1,deftitle);
							this.addLayer(defblocke,1,defbtn);
						}
						else
						{
							var dataloadID = document.getElementById('dbox-fn-dataload');
							var dataLoadAlert = dataloadID.getAttribute('_alertOnInst');
							dbx.Instantiate.fromString(dataloadID.value,dataLoadAlert);
						}

                    dbx.ctrlMenu.createMenu('top',true,'');

                    var isItTouchDevice = 'ontouchstart' in window;

                    if(!isItTouchDevice){
                        document.onmousedown = dbx.Actions.objectDrag;
                        document.onmouseup = dbx.Actions.objectUndrag;
                    } else {
                        document.ontouchstart = dbx.Actions.objectDrag;
                        document.ontouchend = dbx.Actions.objectUndrag;
                    }


					}
			},

			formElement: function(type,id,cls,styl,property){

				obj = null;
				var styles = {hidden:["display","none"],visible:["display","block"],inline:["display","inline-block"]};

				if(obj = document.createElement(type))
				{
					if(id.length > 0){
						obj.id = dboxCheckMarkup(id,1);
					}
					if(cls.length > 0){
						cls = dboxCheckMarkup(cls,1);
						classes = cls.split(' ');

						for(var i in classes){
							if(classes[i].length>0 && i>0)
								obj.className += ' '+classes[i];
							else
								obj.className += classes[i];
						}
					}
					if(typeof styles[styl] === 'object'){
						obj.style.cssText = ''+styles[styl][0]+':'+styles[styl][1]+';';
					}
				}
				return obj;

			},

			formLayer: function(lid,lcls,lstyl,levent,content){
				newLayer = this.formElement(this.dbx().Elements.layerOpen,lid,lcls,lstyl,'');
				newLayer.innerHTML = content;
				return newLayer;
			},

			tableBlock: function(field,lcls,content,title,pos,color,drag){

				oDefId = this.dbx().Elements.tableDefId;
				oClass = this.dbx().Elements.tableRoot;
				oId = oClass+'-'+oDefId;
				oTag = this.dbx().Elements.layerOpen;
				oClassIn = this.dbx().Elements.tableWrap;
                var oLastTable = false;
                var numId = 0;

				if(typeof(field) === 'object'){
					var tObjects = this.dbx().DataModel.FormDataCountChild(field,oTag+':'+oClass);
                    numId = tObjects.length;
                    oLastTable = tObjects[numId-1];
					numId++;
				} else {
					numId=parseInt(field);
				}

                oClass += ' '+lcls;
                if(drag){
                    oClass += ' draggable';
                }

				var newBlock = this.formElement(this.dbx().Elements.layerOpen,oId+numId,oClass,'','');
				newBlock.setAttribute('_idobject',numId);

				if(content.length > 0)
					newBlock.innerHTML = content;

				var newBlockIn = this.formElement(oTag,oClassIn+oDefId+numId,oClassIn,'','');
				newBlockIn.setAttribute('_idobject',numId);
				this.addLayer(newBlock,1,newBlockIn);

				return {tableObj: newBlock, intableObj: newBlockIn, tableId: numId, lasttableObj: oLastTable};
			},

            btnForm: function(lid,lcls,title,attachevent){
                var newButton=false;
                var oDefId = this.dbx().Elements.btnSimple;
                var oClass = this.dbx().Elements.btnSimple + ' ' + lcls;
                var oTag = this.dbx().Elements.layerOpen;
                if(lid!=null && lid.length>1){
                    oDefId=lid;
                }
                newButton = this.formElement(oTag,oDefId,oClass,'','');
                newButton.innerHTML = '<h4>'+title+'</h4>';
                if(typeof(attachevent) === 'function'){
                   newButton.addEventListener('click',attachevent,false);
                }
                return newButton;
            },

			btnAdd: function(lid,lcls,applyobj){
				lcls = this.dbx().Elements.btnAdd+' '+lcls;
				var newButton = this.formElement(this.dbx().Elements.layerOpen,lid,lcls,'','');
				newButton.addEventListener('click',function(e){dboxClass().Actions.addField(applyobj,newButton);},false);
				return newButton;
			},

			btnOk: function(lid,lcls,applyobj,parentobj){
				var newOkbtn = this.formElement(this.dbx().Elements.layerOpen,lid,'dbox-fn-button-ok-calm '+lcls+'','','');
				newOkbtn.addEventListener('click',function(e){dboxClass().Actions.applyField(applyobj,parentobj);},false);
				return newOkbtn;
			},

			btnCancel: function(lid,lcls,applyobj){
				var newCancel = this.formElement(this.dbx().Elements.layerOpen,lid,'dbox-fn-button-cancel-calm '+lcls+'','','');
				newCancel.addEventListener('click',function(e){dboxClass().Actions.closeField(applyobj);},false);
				return newCancel;
			},

			titleAdd: function(lid,lcls,value){
				lcls = this.dbx().Elements.tableTitle+' '+lcls;
				var newTitle = this.formElement(this.dbx().Elements.layerOpen,lid,lcls,'','');
				newTitle.innerHTML = '<h4>'+value+'</h4>';
				newTitle.addEventListener('click',function(e){dboxClass().Actions.editField(newTitle,1);},false);
				return newTitle;
			},

			inputAdd: function(lid,lcls,value,validate,state,submit,submitForm){

				if(!submit) {
					newInput = this.formElement(this.dbx().Elements.inputOpen,lid,'dbox-fn-modal-window-frame-form-input '+lcls+'',state,'');
					newInput.value = value;
				} else {
					newInput = this.formElement(this.dbx().Elements.inputOpen,lid,'dbox-fn-button-ok-calm form-submit '+lcls+'',state,'');
					newInput.setAttribute('type','submit');
					newInput.setAttribute('value','');
					newInput.addEventListener('click',function(e){dboxClass().Actions.applyForm(submitForm);},false);
				}
				return newInput;
			},

			selectAdd: function(lid,lcls,data,format,defvalue){

				newSelect = this.formElement(this.dbx().Elements.selectOpen,lid,'dbox-fn-data-form-select '+lcls+'','','');

				if(defvalue == undefined){
					defvalue = '';
				}

				if(data.length > 0){
					data = data.split(',');
					var cont = '';
					if(format){
						for(var idx in data)
						{
							if(data[idx] == defvalue){
								cont += '<option selected="selected" value="'+data[idx]+'">'+data[idx]+'</option>';
							} else {
								cont += '<option value="'+data[idx]+'">'+data[idx]+'</option>';
							}
						}
					} else {
						for(var idx in data)
						{
							cont += '<option value="'+idx+'">'+data[idx]+'</option>';
						}
					}
					newSelect.innerHTML = cont;
				}
				return newSelect;
			},

			formAdd: function(lid,lcls,data){
				newForm = this.formElement(this.dbx().Elements.formOpen,lid,'dbox-fn-data-form-obj '+lcls+'','','');
				return newForm;
			},

			labelAdd: function(lid,lcls,name){
				newLabel = this.formElement(this.dbx().Elements.layerOpen,lid,'dbox-fn-data-form-label '+lcls+'','','');
				if(name.length > 0){
					newLabel.innerHTML = '<h4>'+name+'</h4>';
				}
				return newLabel;
			},

			textAdd: function(lid,lcls,text){
				newText = this.formElement(this.dbx().Elements.textOpen,lid,'dbox-fn-data-form-label '+lcls+'','','');
				newText.innerHTML = text;
				return newText;
			},

			addLayer: function(object,addtype,child){
				if(addtype==0)
				{
					document.body.appendChild(child);
				}
				else
				{
					object.appendChild(child);
				}
			}
		}

		//	ACTIONS consist from functions which binds to event JS model

		this.Actions = {

			// Adding field // obj = ParentObject to Inititator // objParent = inititator
			addField: function(obj,objParent){

				dbox = new dboxClass();

				var content = '{"input":{"ititle":"Parameter name","ident":"'+obj.id+'form-input","classname":"dbox-model-data","type":"visible","ivalue":""},"select":{"ititle":"Parameter type","ident":"'+obj.id+'form-select","classname":"dbox-model-data","type":"visible","ivalue":"int,smallint,tinyint,float,varchar,text,mediumtext"},"text":{"ititle":"Parameter descrition","ident":"'+obj.id+'form-text","classname":"dbox-model-data","type":"visible","ivalue":""},"service":{"submit":"okcancel"}}';

				formData = dbox.DataModel.FormDataSet(obj,content,null,false,obj.id);

				formData.className += ' dbox-table-element';

				obj.insertBefore(formData,objParent);

			},

			editElement: function(obj,objParent){

				dbox = new dboxClass();
				idObject = obj.getAttribute('_idobject');

				var content = '{"input":{"ititle":"Parameter name","ident":"'+obj.id+'form-input","classname":"dbox-model-data","type":"visible","ivalue":0},"select":{"ititle":"Parameter type","ident":"'+obj.id+'form-select","classname":"dbox-model-data","type":"visible","ivalue":"int,smallint,tinyint,float,varchar,text,mediumtext","idefault":1},"text":{"ititle":"Parameter descrition","ident":"'+obj.id+'form-text","classname":"dbox-model-data","type":"visible","ivalue":2},"service":{"submit":"okcancel"}}';

				var datacont = dbox.DataModel.FormDataTraverse(obj,'div:dbox-table-el-data');

				formData = dbox.DataModel.FormDataSet(obj,content,datacont,idObject,objParent.id);

				formData.className += ' dbox-table-element';

				obj.parentNode.replaceChild(formData,obj);

			},

			// Set up modal window to form with data values
			editField: function(e,data){

				data=parseInt(data);
				if(data == 1)
				{
					content = '{"conttype":"input","confirm":"ok","contents":"'+ e.innerHTML +'"}';
				}
				checkObj = document.getElementById(e.id+'modal');
				if(checkObj == null)
				{
					dbox = new dboxClass();
					newWindow = dbox.Windows.modalMutable(e,content,true);
					newClose = dbox.Windows.modalBtnClose(e,newWindow);
					dbox.Markup.addLayer('',0,newWindow);
					dbox.Markup.addLayer(newWindow,1,newClose);
					dbox.Windows.modalPosition(e,newWindow);
				}

			},

			// Change single field value
			applyField: function(obj,objParent){

				dbox = new dboxClass();
				newData = dbox.Windows.modalFormSelect(objParent);

				if(newData.length > 0){
					if(newData[1].length > 0){
						data = document.createElement(newData[1]);
						data.innerHTML = dboxJsonTextCheck(newData[0]);
						obj.innerHTML = '';
						dbox.Markup.addLayer(obj,1,data);
					}
					else{
						data = dboxJsonTextCheck(newData[0]);
						obj.innerHTML = data;
					}
				}
				this.closeField(objParent);
			},

			// Apply form data
			applyForm: function(obj){

				var newData = null;

				var formatOutput = '';
				formatOutput += '{"1":{"ident":"dbox-table-el","itype":"root","iclass":"dbox-table-element","innertag":"","icontent":""},';
				formatOutput += '"2":{"ident":"dbox-table-el-title","itype":"visible","iclass":"dbox-table-el-data","innertag":"h4","icontent":0},';
				formatOutput +=	'"3":{"ident":"dbox-table-el-type","itype":"visible","iclass":"dbox-table-el-data","innertag":"h5","icontent":1},';
				formatOutput +=	'"4":{"ident":"dbox-table-el-text","itype":"notes","iclass":"dbox-table-el-data","innertag":"","icontent":2}}';

				dbox = new dboxClass();
				event.preventDefault ? event.preventDefault() : event.returnValue = false;

				var formParent = document.getElementById(obj.getAttribute('formparent'));
				var formForceId = obj.getAttribute('_idobject');

				newData = dbox.DataModel.FormDataGet(obj,formParent,formatOutput,formForceId);

				var pNode = obj.parentNode;

				pNode.replaceChild(newData,obj);

			},

			showDescription: function(obj){

				var showid = obj.id+'desc';
				var idobj = document.getElementById(showid);
				var data = idobj.innerHTML;
				var content = '{"conttype":"inform","confirm":"ok","contents":"'+ data +'"}';

				checkObj = document.getElementById(obj.id+'modal');
				if(checkObj == null)
				{
					dbox = new dboxClass();
					newWindow = dbox.Windows.modalSimple(obj,content,true);
					newClose = dbox.Windows.modalBtnClose(obj,newWindow);
					dbox.Markup.addLayer('',0,newWindow);
					dbox.Markup.addLayer(newWindow,1,newClose);
					dbox.Windows.modalPosition(obj,newWindow);
				}

			},

			closeField: function(e,data){
				e.parentNode.removeChild(e);
			},

			addTable: function(){

                dbox = new dboxClass();
                var tableIn=false;
                var tableOut=false;
                var tableId=false;

                var worklayer = dboxGetObjectByClass('dbox-fn-work')[0];

                var tableObject = dbox.Markup.tableBlock(worklayer,'','','','','',true);
                tableIn=tableObject.intableObj;
                tableOut=tableObject.tableObj;
                tableId=tableObject.tableId;
                var tableLast=tableObject.lasttableObj;

                var titleObject = dbox.Markup.titleAdd(dbox.Elements.tableTitle+'-'+dbox.Elements.tableDefId+tableId,dbox.Elements.tableTitle,dbox.Elements.tableDefId);
                var buttonObject = dbox.Markup.btnAdd(dbox.Elements.btnAdd+'-'+dbox.Elements.tableDefId+tableId,dbox.Elements.btnAdd,tableIn);

                if(tableLast){
                lastElWidth = dboxCoordsReplacePx(window.getComputedStyle(tableLast,null).getPropertyValue('width'));
                lastElTop = tableLast.offsetTop;
                lastElLeft = tableLast.offsetLeft + lastElWidth + 20;
                tableOut.style.left = lastElLeft;
                tableOut.style.top = lastElTop;
                }

                dbox.Markup.addLayer(tableIn,1,titleObject);
                dbox.Markup.addLayer(tableIn,1,buttonObject);
                dbox.Markup.addLayer(worklayer,1,tableOut);

			},

			// Dragging draggable object
			objectDrag: function(e){

                var isTouch = 'ontouchstart' in window;
				var target;
				var candrag = false;

				// Checks for IE as Mozilla MDN describes
				if (e == null) {
					e = window.event;
				}

				// Checks for target as MDN describes for
				// old IE vesions of event handler
        		if(e.target != null){
        			target = e.target;
        		} else {
        			target = e.srcElement;
        		}

        		// Checking class by parsing split and compare
        		// In our environment only absolute layers can
        		// have draggable option (modals, tables, etc)
        		if(dboxReturnClass(target.className,'draggable')){
        			candrag = true;
        		}

        		// If left or middle button is pressed
        		// As MDN describes old IE have leftbtn = 1
        		if(e.button == 0 && candrag || e.button == 1 && candrag || isTouch && candrag){

        			// Get object offset which we plus mouseDelta
        			ox = dboxCoordsReplacePx(target.style.left);
        			oy = dboxCoordsReplacePx(target.style.top);

        			// If style bugging for some reasons
        			// we use fast check for Number exists
        			if(ox != ox * 1){
        				ox=0;
        				oy=0;
        			}

        			// Setup global variable for undrag object
        			// On mouseUp event and setup cursor style change
        			$dboxDragObject = target;
        			$dboxDragObject.style.cursor = 'pointer';

        			// Execute mathematics with mouseDelta offset

                    if(!isTouch){
                        // Get starting points for count shift of mouse
                        mx = e.clientX;
                        my = e.clientY;
                        document.onmousemove = dboxDragObject;
                    } else {
                        // Get starting points for count shift of finger
                        var tStart = e.changedTouches[0];
                        mx = tStart.clientX;
                        my = tStart.clientY;
                        document.ontouchmove = dboxDragObjectTouch;
                    }
        		}
        		//return false;
			},

			objectUndrag: function(e){
				if($dboxDragObject != null){
					$dboxDragObject.style.cursor = 'default';
				}
				$dboxDragObject = null;
                var isTouch = 'ontouchstart' in window;
                if(!isTouch){
                    document.onmousemove = null;
                } else {
                    document.ontouchmove = null;
                }
			}
		}

		// MODAL WINDOWS AND EDITABLE SUB-ELEMENTS CREATION
		this.Windows = {

			modalSimple: function(obj,content,drag){
				dbox = new dboxClass();
				oClass = dbox.Elements.windowModal;
				if(drag){
					oClass += ' draggable';
				}

				newWindow = dbox.Markup.formElement(dbox.Elements.layerOpen,obj.id + 'modal',oClass,'','');
				newContent = this.modalFormData(obj,content,newWindow);

				dbox.Markup.addLayer(newWindow,1,newContent);
				return newWindow;
			},

			modalMutable: function(obj,content,drag){
				dbox = new dboxClass();
				oClass = dbox.Elements.windowModal;
				if(drag){
					oClass += ' draggable';
					//newTitle.addEventListener('mousedown',function(e){dboxClass().Actions.editField(newTitle,1);},false);
				}
				newWindow = dbox.Markup.formElement(dbox.Elements.layerOpen,obj.id + 'modal',oClass,'','');
				newContent = this.modalFormData(obj,content,newWindow);

				dbox.Markup.addLayer(newWindow,1,newContent);
				return newWindow;
			},

			modalBtnClose: function(obj,modalParent){
				dbox = new dboxClass();
				newClose = dbox.Markup.formElement(dbox.Elements.layerOpen,obj.id + 'modalend',dbox.Elements.windowModalClose,'','');
				newClose.addEventListener('click',function(e){dboxClass().Actions.closeField(modalParent,1);},false);
				return newClose;
			},

			modalFormData: function(obj,data,modalParent){
				var dbox = new dboxClass();
				var newForm = dbox.Markup.formElement(dbox.Elements.layerOpen,obj.id + 'modal-form','dbox-fn-modal-window-frame-form','','');
				var object = JSON.parse(data);
				if(object.conttype == "input")
				{
					contents = dboxCheckFormat(object.contents);
					formInput = dbox.Markup.inputAdd(obj.id + 'form-input','dbox-modal-data',contents,false,"visible");
					dbox.Markup.addLayer(newForm,1,formInput);
					contents = dboxReturnFormat(object.contents);
					formInput = dbox.Markup.inputAdd(obj.id + 'form-input-format','dbox-modal-format',contents,false,"hidden");
					dbox.Markup.addLayer(newForm,1,formInput);
					if(object.confirm == "ok")
					{
						subDiv = document.createElement('center');
						formSubmit = dbox.Markup.btnOk(obj.id + 'form-submit','',obj,modalParent);
						dbox.Markup.addLayer(newForm,1,subDiv);
						dbox.Markup.addLayer(subDiv,1,formSubmit);
					}
				}

				if(object.conttype == "inform")
				{
					newText = dbox.Markup.formElement(dbox.Elements.layerOpen,obj.id + 'modal-text','dbox-fn-modal-window-frame-text','','');
					newText.innerHTML = object.contents;
					dbox.Markup.addLayer(newForm,1,newText);
					if(object.confirm == "ok")
					{
						subDiv = document.createElement('center');
						formSubmit = dbox.Markup.btnOk(obj.id + 'form-submit','',obj,modalParent);
						dbox.Markup.addLayer(newForm,1,subDiv);
						dbox.Markup.addLayer(subDiv,1,formSubmit);
					}
				}

				return newForm;
			},

			// Single input selection with sub markup selection
			modalFormSelect: function(obj){

				var doReturn = false;
				dbox = new dboxClass();
				d = ["",""];
				s = dbox.DataModel.FormDataTraverse(obj,null);

				for(i=0; i<s.length; i++){
					c = false;
					f = false;
					ce = s[i];
					cn = ce.className;
					c = dboxReturnClass(cn,'dbox-modal-data');
					f = dboxReturnClass(cn,'dbox-modal-format');

					if(c != false){
						d[0] = ce.value;
						doReturn=true;
					}
					if(f != false){
						d[1] = ce.value;
					}
				}
				if(doReturn)
					return d;
				else
					return false;
			},

			modalPosition: function(obj,forobj){

				mtop = getComputedStyle(obj,null).getPropertyValue('margin-top');
				mleft = getComputedStyle(obj,null).getPropertyValue('margin-left');
				coordl = obj.offsetLeft + dboxCoordsReplacePx(mleft);
				coordt = obj.offsetTop + dboxCoordsReplacePx(mtop);

				do {
					coordl += obj.offsetLeft;
					coordt += obj.offsetTop;
				} while (obj = obj.offsetParent)

				if(coordl < 33)
					coordl = 32;
				if(coordt < 33)
					coordt = 32;

				forobj.style.left = coordl;
				forobj.style.top = coordt;
			}
		}

		this.DataModel = {

			FormDataSet: function(obj,dataobject,dataforms,forceid,forceparent) {

				var ret = '';
				var data = JSON.parse(dataobject);
				var insertdata = [];
				newForm = dbox.Markup.formAdd(obj.id+'form-obj','','');

				if(!forceid || forceid == undefined){
					forceid=this.FormDataCountChild(obj,'div:dbox-table-element').length;
					forceid+=''+obj.getAttribute('_idobject');
				}

				console.log(forceid + ' / ' + obj.id);
				newForm.setAttribute('formparent',forceparent);
				newForm.setAttribute('_idobject',forceid);

				if(dataforms != null && dataforms.length > 0)
				{
					for (var p in dataforms){
						curcont = dboxCheckFormat(dataforms[p].innerHTML);
						insertdata.push(curcont);
					}
				}

				var i=0;
				for (var p in data){

					if(p == "input"){
						labelElement = dbox.Markup.labelAdd(data[p].ident+'label'+i,'',data[p].ititle);
						dbox.Markup.addLayer(newForm,1,labelElement);

						if(insertdata[data[p].ivalue]){
							formElement = dbox.Markup.inputAdd(data[p].ident,data[p].classname,insertdata[data[p].ivalue],false,"visible");
						} else {
							formElement = dbox.Markup.inputAdd(data[p].ident,data[p].classname,data[p].ivalue,false,"visible");
						}

						dbox.Markup.addLayer(newForm,1,formElement);
					}

					if(p == "select"){
						labelElement = dbox.Markup.labelAdd(data[p].ident+'text'+i,'',data[p].ititle);
						dbox.Markup.addLayer(newForm,1,labelElement);

						if(insertdata[data[p].idefault]){
							formElement = dbox.Markup.selectAdd(data[p].ident,data[p].classname,data[p].ivalue,true,insertdata[data[p].idefault]);
						} else {
							formElement = dbox.Markup.selectAdd(data[p].ident,data[p].classname,data[p].ivalue,true);
						}

						dbox.Markup.addLayer(newForm,1,formElement);
					}

					if(p == "text"){
						labelElement = dbox.Markup.labelAdd(data[p].ident+'text'+i,'',data[p].ititle);
						dbox.Markup.addLayer(newForm,1,labelElement);

						if(insertdata[data[p].ivalue]){
							formElement = dbox.Markup.textAdd(data[p].ident,data[p].classname,insertdata[data[p].ivalue]);
						} else {
							formElement = dbox.Markup.textAdd(data[p].ident,data[p].classname,data[p].ivalue);
						}

						dbox.Markup.addLayer(newForm,1,formElement);
					}

					if(p == "service"){
						if(data[p].submit == "ok"){
							formElement = dbox.Markup.inputAdd(obj.id+'form-submit','','',false,"visible",true);
							dbox.Markup.addLayer(newForm,1,formElement);
						}
						if(data[p].submit == "okcancel"){

							subDiv = document.createElement('div');
							subDiv.setAttribute('id',obj.id+'form-submit-center');
							formElement = dbox.Markup.inputAdd(obj.id+'form-submit','','',false,"inline",true,newForm);
								dbox.Markup.addLayer(subDiv,1,formElement);

							formElement = dbox.Markup.btnCancel(obj.id+'form-cancel','form-cancel',newForm);
								dbox.Markup.addLayer(subDiv,1,formElement);

							dbox.Markup.addLayer(newForm,1,subDiv);

						}
					}
					i++;
				}
				return newForm;
			},

			FormDataGet: function(obj,parentObj,model,forceid){

			// 	'{"1":{"ident":"dbox-table-el","itype":"root","iclass":"dbox-table-element","innertag":"","icontent":""},
			//	"2":{"ident":"dbox-table-el-title","itype":"visible","iclass":"dbox-table-el-data","innertag":"h4","icontent":0},
			//	"3":{"ident":"dbox-table-el-type","itype":"visible","iclass":"dbox-table-el-data","innertag":"","icontent":1},
			//	"4":{"ident":"dbox-table-el-type","itype":"visible","iclass":"dbox-table-el-data","innertag":"","icontent":2}}';

			var idx = null;

			if(forceid == "null"){
				forceid = null;
				var idx = 1;
			} else {
				idx = forceid;
			}

			var d = [];
			var n = false;

			var f = JSON.parse(model);

			if(obj!=null){
				var o = this.FormDataTraverse(obj,null);
				for(var i=0; i<o.length; i++){
						var c = false;
						ce = o[i];
						cn = ce.className;

						if(dboxReturnClass(cn,'dbox-model-data')){
							d.push(ce);
						}
					}
			}

			if(parentObj!=null && forceid == null){
				idx = this.FormDataTraverse(parentObj,'div:dbox-table-element').length;
			}

			for (var p in f){

					elemCont = '';

					if(obj!=null){
						no = parseInt(f[p].icontent);
						if(d[no])
							elemCont = dboxJsonTextCheck(d[parseInt(f[p].icontent)].value);
					} else {
						elemCont = f[p].icontent;
					}

					if(f[p].itype == 'root'){
						n = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx,f[p].iclass,'','');
					}

					if(f[p].itype == 'visible'){

						var ne = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx,f[p].iclass,'','');

						if(f[p].innertag.length > 0){
							ne = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx,f[p].iclass,'','');
							nf = document.createElement(f[p].innertag);
							nf.innerHTML = elemCont;
							dbox.Markup.addLayer(ne,1,nf);
						} else {
							ne.innerHTML = elemCont;
						}
						dbox.Markup.addLayer(n,1,ne);
					}

					if(f[p].itype == 'notes'){

						var ne = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx +'desc',f[p].iclass,'','');
						var ni = null;

						if(f[p].innertag.length > 0){
							ne = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx,f[p].iclass,'','');
							nf = document.createElement(f[p].innertag);
							nf.innerHTML = elemCont;
							dbox.Markup.addLayer(ne,1,nf);
						} else {
							ne.innerHTML = elemCont;
						}

						ne.style.display = "none";

						ni = dbox.Markup.formElement(dbox.Elements.layerOpen,f[p].ident + idx,'dbox-fn-button-desc-calm','','');
						ni.addEventListener('click',function(e){dbox.Actions.showDescription(ni);},false);

						dbox.Markup.addLayer(n,1,ne);
						dbox.Markup.addLayer(n,1,ni);

					}
				}

			var ed = null;
			ed = dbox.Markup.formElement(dbox.Elements.layerOpen,'dbox-edit-el' + idx,'dbox-fn-button-microedit-calm','','');
			ed.addEventListener('click',function(e){dbox.Actions.editElement(n,parentObj);},false);

			dbox.Markup.addLayer(n,1,ed);

			n.setAttribute("_idobject",idx);

			return n;
			},

			// Method go deeps in structure of created form
			// And returns massive of input elements from all
			// Child objects of root given object

			FormDataTraverse: function(obj,objclass){

                var t=['INPUT','SELECT','TEXTAREA'];
                var cl=null;

				if(objclass != null){
					objclass = objclass.split(':');
					t=[objclass[0].toUpperCase()];
					cl=objclass[1];
				}

				var d=[];
				var s=["none",obj.id];
				var c=obj.childNodes;
				var k=c.length;
				var z=0;
				var stopID = obj.parentNode.id;

				for(var i=0;i<k;i++){

					cur = c[i];
					ntype = cur.nodeType;

					if(ntype != 3)
					{
						if(cur.id == null || cur.id == '') {
							ti = new Date().getTime();
							cur.setAttribute('id','dbox-fdt' + ti);
						}

						if(s.indexOf(cur.id) < 0) {
							// Did we need it? Value can be array if needed
							if(t.indexOf(cur.tagName) == -1) {
								// If it can have childs lets try to get them
								curc = dboxReturnNodes(cur);
								if(curc.length > 0) {
									// Return cycle to initial state
									c=curc;
									i=-1;
									k=c.length;
								}
							}
							else {
							// If we find valuable element add it to return
							// Check if it query only for concrete elements and class
								if(cl != null){
									if(dboxReturnClass(cur.className,cl)){
										d.push(cur);
									}
								} else {
							// Or it go search for standard input types
									d.push(cur);
								}
							}
						}
					// Add passed ID to seen massive
					s.push(cur.id);
					}
					else
					{
					// If we finds a weird #text node we immidately go to parent of its parent
						moveUp = cur.parentNode.parentNode;
						c=dboxReturnNodes(moveUp);
						i=-1;
						k=c.length;
						if(k != k * 1){
							k=1;
						}
					}

					// If we got end element go back to parent
					if(i==(k-1))
					{
						// And return cycle to initial state
						moveUp = cur.parentNode.parentNode;
						c=dboxReturnNodes(moveUp);
						i=-1;
						k=c.length;
						if(moveUp.id == stopID)
							break;
					}

				z++;
				if(z>1000)
					break;
				}
				return d;
			},

			FormDataCountChild: function(obj,objclass){

				t=[];
				cl=null;

				if(objclass != null){
					objclass = objclass.split(':');
					var t=[objclass[0].toUpperCase()];
					var cl=objclass[1];
				}

				var d=[];
				var s=["none",obj.id];
				var c=obj.childNodes;
				var k=c.length;

				for(var i=0;i<k;i++){

					cur = c[i];
					ntype = cur.nodeType;

					if(cur.id == null || cur.id == '') {
							ti = new Date().getTime();
							cur.setAttribute('id','dbox-fdt' + ti);
						}

					if(ntype != 3)
					{
						if(t.length > 0){
							if(dboxReturnClass(cur.className,cl)){
								d.push(cur);
							}
						} else {
							d.push(cur);
						}
					}
				}

				return d;
			}
		}

		// VISUAL CONSTRUCTOR INSTANTIATIONS

		this.Instantiate = {

			exampleNote: '<h3> This form was created from a JSON string below ( this window is draggable ) </h3> ',

			exampleObj: function(){
				var Obj = '{';

					Obj += '"0":{';
					Obj += '"title":"Example Title",';
					Obj += '"elements":{';
						Obj += '"0":{"text":"exampleID","select":"int","notes":"This field is for exampleID and Not for anything else, \\n but you must know that you can move this form element \\n just by grab and drag it by its corners "},';
						Obj += '"1":{"text":"exampleTitle","select":"varchar","notes":"This field is little diferent from above field"},';
						Obj += '"2":{"text":"exampleFloat","select":"float","notes":"This field is third in this example"},';
						Obj += '"3":{"text":"Text_field", "select":"text","notes":"This field is fourth and also a text field in this example \\n <more complex field as you see>"}';
					Obj += '},';
					Obj += '"position":"50,50"';
					Obj += '},';

					Obj += '"1":{';
					Obj += '"title":"Secondary Table",';
					Obj += '"elements":{';
						Obj += '"0":{"text":"indexID","select":"int","notes":"Pkey for secondary table, \\n you must know that you can move this form element \\n just by grab and drag it by its corners "},';
						Obj += '"1":{"text":"newTitle","select":"varchar","notes":"In the secondary table its like something same like in a first table"},';
						Obj += '"2":{"text":"MoneyCost","select":"float","notes":"Another third field"},';
						Obj += '"3":{"text":"MoneyDesc", "select":"text","notes":"Let it be text too \\n <text text text and text>"}';
					Obj += '},';
					Obj += '"position":"50,350"';
					Obj += '}';

				Obj += '}';

				return Obj;
			},

			fromEntity: function(){

			},

			fromString: function(stringObj,alertObj){

				var dbox = new dboxClass();
					if(stringObj == null || stringObj == undefined)
						stringObj = this.exampleObj();

				var docComplete = false;
				var dataloadID = false;

				if(document.readyState !== "complete"){
					dataloadID = document.getElementById('dbox-fn-dataload');
				}
				else{
					docComplete = true;
				}

				if(dataloadID){
					dataloadID.value = stringObj;
					dataloadID.setAttribute('_alertOnInst',alertObj);
				}

				if(docComplete){

					var worklayer = dboxGetObjectByClass('dbox-fn-work')[0];
					var parseres = this.instParse(stringObj);

					if(parseres && typeof(worklayer) !== 'undefined'){
						for(var i=0; i<parseres.length; i++){
							dbox.Markup.addLayer(worklayer,1,parseres[i]);
						}

						if(alertObj)
						{
							content = '{"conttype":"inform","confirm":"ok","contents":"'+ dboxJsonTextCheck(this.exampleNote+stringObj) +'"}';
							checkObj = document.getElementById(worklayer.id+'modal');
							if(checkObj == null)
							{
								newWindow = dbox.Windows.modalSimple(worklayer,content,true);
								newClose = dbox.Windows.modalBtnClose(worklayer,newWindow);
								dbox.Markup.addLayer('',0,newWindow);
								dbox.Markup.addLayer(newWindow,1,newClose);
								dbox.Windows.modalPosition(worklayer,newWindow);
							}
						}
					}
				}
			},

			instParse: function(jsonObject){

				dbox = new dboxClass();
				var mainForm = false;
				var formBtn = false;
				var dobj;
                var d=false;
                var mFormIn=false;
                var mFormId=false;
                var mFormOut=false;

                if(dobj = JSON.parse(jsonObject)){
                    d=[];

                    for (var p in dobj){

                        p = parseInt(p);

                        mainForm = dbox.Markup.tableBlock(p,'','','','','',true);
                        mFormIn = mainForm.intableObj;
                        mFormId = mainForm.tableId;
                        mFormOut = mainForm.tableObj;

                            iobj=dobj[p];

                            for (var k in iobj){

                                if(k == "title"){
                                    nt = dbox.Markup.titleAdd(dbox.Elements.tableTitle+'-'+dbox.Elements.tableDefId+p,dbox.Elements.tableTitle,iobj[k]);
                                    dbox.Markup.addLayer(mFormIn,1,nt);
                                }

                                if(k == "elements"){

                                    var ne = null;
                                    var formatOutput='';
                                    eobj = iobj[k];

                                    for (var o in eobj){

                                        formForceId = o+''+p;
                                        ineo=eobj[o];
                                        formatOutput = '{"1":{"ident":"dbox-table-el","itype":"root","iclass":"dbox-table-element","innertag":"","icontent":""},';
                                        ecnt=2;

                                        for (var x in ineo)
                                        {
                                            if(x=='text'){
                                                formatOutput += '"'+ecnt+'":{"ident":"dbox-table-el-title","itype":"visible","iclass":"dbox-table-el-data","innertag":"h4","icontent":"'+dboxJsonTextCheck(ineo[x])+'"},';
                                                }
                                            if(x=='select'){
                                                formatOutput +=	'"'+ecnt+'":{"ident":"dbox-table-el-type","itype":"visible","iclass":"dbox-table-el-data","innertag":"h5","icontent":"'+dboxJsonTextCheck(ineo[x])+'"},';
                                            }
                                            if(x=='notes'){
                                                formatOutput +=	'"'+ecnt+'":{"ident":"dbox-table-el-text","itype":"notes","iclass":"dbox-table-el-data","innertag":"","icontent":"'+dboxJsonTextCheck(ineo[x])+'"}';
                                            }
                                            ecnt++;
                                        }

                                        formatOutput +=	'}';

                                        ne = dbox.DataModel.FormDataGet(null,mFormIn,formatOutput,formForceId);
                                        dbox.Markup.addLayer(mFormIn,1,ne);
                                    }
                                }
                                if(k == "position"){

                                    var objPos = iobj[k].split(',');

                                    objPosT = parseInt(objPos[0]);
                                    objPosL = parseInt(objPos[1]);

                                    mFormOut.style.left = objPosL;
                                    mFormOut.style.top = objPosT;
                                }
                            }
                        formBtn = dbox.Markup.btnAdd(dbox.Elements.btnAdd+'-'+dbox.Elements.tableDefId+mFormId,dbox.Elements.btnAdd,mFormIn);
                        dbox.Markup.addLayer(mFormIn,1,formBtn);
                        d.push(mFormOut);
                    }
                }
			return d;
			}
		}

		return this;
	}

	$dbox.fn = dboxClass.prototype =
	{
		

	}

	// Checking the properties for proper work of ID
	function dboxCheckMarkup(text,type){
		var identtype = /[^a-zA-Z0-9\s_\-]+/g;
		if(type==1){
			txt = text.replace(identtype,"");
			return txt;
		}
	}

	// JSON TEXT CHECK FOR QOUTES
	function dboxJsonTextCheck(jt){
	var safeT = {'&':'&amp;','<':' &lt;','>':'&gt; ','\'':'\x27','"':'\\\x22','\\':' ','<h3>':'<h3>','</h3>':'</h3>'};
	jt = jt.replace(/[\\\"'&]|(<h3>)|(<\/h3>)|(<)|(>)/ig,function(e){return safeT[e] || e;});
	jt = jt.replace(/\r?\n/g,'<br />');
	return jt;
	}

	// Do some routine for 100% extraction number from style
	function dboxCoordsReplacePx(t){
		var r = /[(px+)(%+)]+/g;
		return parseInt(t.replace(r,''));
	}

	// Cut string from formatting tags
	function dboxCheckFormat(text){
		return text.replace(/<[^>]*>/g,'');
	}

	// Extract string format tag to name
	function dboxReturnFormat(c){
		var t = ''; var w = false;
		for(var i=0; i<c.length; i++){
			if(c[i] == ">" || c[i] == " ")break;if(w)t += c[i];if(c[i] == "<")w = true;
		} return t;
	}

	// Return expected class or false
	function dboxReturnClass(s,c){
		var r=false;
		if(typeof(s)=='string'){s=s.split(' ');
		for(var i=0;i<s.length;i++){
			if(s[i]==c){r=s[i];}}
		}
		return r;
	}

	function dboxGetObjectByClass(objClassName){
		var d=[];
	    var elements = document.getElementsByTagName("*");
	    for (var k = 0; k < elements.length; k++){
	    	if(dboxReturnClass(elements[k].className,objClassName)){
	    		d.push(elements[k]);
	    	}
	    }
	    return d;
	}

	// Some predefined notation
	function dboxReturnNodes(obj){
		return obj.childNodes;
	}

	// Simple math for counting mouseDelta in Dragging
	function dboxDragObject(e){
		if (e == null) {
			e = window.event;
		}
		if($dboxDragObject!=null){
			$dboxDragObject.style.left = (ox + e.clientX - mx) + 'px';
			$dboxDragObject.style.top = (oy + e.clientY - my) + 'px';
		}
        e.preventDefault();
	}
    // Simple math for counting mouseDelta in Dragging with Touch
    function dboxDragObjectTouch(e){
        if (e == null) {
            e = window.event;
        }
        tStart = e.changedTouches[0];
        if($dboxDragObject!=null){
            $dboxDragObject.style.left = (ox + tStart.clientX - mx) + 'px';
            $dboxDragObject.style.top = (oy + tStart.clientY - my) + 'px';
        }
        e.preventDefault();
    }

if(!window.$dbox) {window.$dbox = $dbox;}
})();